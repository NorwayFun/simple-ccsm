# Georgian translation for simple-ccsm
# Copyright (C) 2023 launchpad.net/compiz
# This file is distributed under the same license as the simple-ccsm package.
# Temuri Doghonadze <temuri.doghonadze@gmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: simple-ccsm\n"
"Report-Msgid-Bugs-To: https://gitlab.com/compiz/simple-ccsm/issues\n"
"POT-Creation-Date: 2017-12-09 01:20+0300\n"
"PO-Revision-Date: 2023-02-16 11:18+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <(nothing)>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

msgid "Minimal"
msgstr "მინიმალური"

msgid "Medium"
msgstr "საშუალო"

msgid "Advanced"
msgstr "დამატებითი"

msgid "Ultimate"
msgstr "უკანასკნელი"

msgid "Provides a simple desktop environment with very few effects."
msgstr "უზრუნველყოფს მარტივ სამუშაო მაგიდის გარემოს ცოტა ეფექტებით."

msgid ""
"Provides good balance between attractiveness and moderate performance "
"requirements."
msgstr "კარგი ბალანსი მიმზიდველობასა და საშუალო წარმადობის მოთხოვნებს შორის."

msgid "Provides more aesthetically pleasing set of effects."
msgstr "მეტი ესთეტიკურად სასიამოვნო ეფექტების ნაკრები."

msgid ""
"Provides very advanced and eye-catching set of effects. Requires faster "
"graphics card."
msgstr ""
"დამატებითი თვალწარმტაცი ეფექტების ნაკრები. აუცილებელია სწრაფი ვიდეობარათის "
"ქონა აუცილებელია."

msgid "Enabled"
msgstr "ჩაირთო"

msgid "Disabled"
msgstr "გამორთულია"

msgid "Zoom"
msgstr "გადიდება"

msgid "Colorfilter"
msgstr "ფერისფილტრი"

msgid "None"
msgstr "არაფერი"

msgid "Can't find the animation plugin."
msgstr "ანიმაციის დამატება ვერ ვიპოვე."

msgid "Can't find any animation extension plugins."
msgstr "ანიმაციის გაფართოების დამატებები ვერ ვიპოვე."

# berylog: Es refereix al plugin Cover per això no faig la traducció
#, c-format
msgid "%s (Cover)"
msgstr "%s (ყდა)"

#, c-format
msgid "%s (Flip)"
msgstr "%s (გადაბრუნება)"

msgid "Cover"
msgstr "ყდა"

msgid "Flip"
msgstr "გადაბრუნება"

msgid ""
"Desktop effects are not supported on your current hardware / configuration. "
"Would you like to cancel enabling of desktop effects or run them anyway?"
msgstr ""
"თქვენი ამჟამინდელი აპარატურით/კონფიგურაციის სამუშაო მაგიდის ეფექტები "
"მხარდაჭერილი არაა. გნებავთ, გააუქმოთ სამუშაო მაგიდის ეფექტები, თუ მაინც "
"გაუშვებთ მათ?"

msgid "_Cancel"
msgstr "&გაუქმება"

msgid "Default"
msgstr "ნაგულისხმები"

msgid "Simple CompizConfig Settings Manager"
msgstr "პარამეტრების მარტივი მმართველი CompizConfig"

msgid "_Enable desktop effects"
msgstr "_სამუშაო მაგიდის ეფექტების ჩართვა"

msgid "Profile:"
msgstr "პროფილი:"

msgid "<b>Description</b>"
msgstr "<b>აღწერა</b>"

msgid "<b>Animations</b>"
msgstr "<b>ანიმაციები</b>"

msgid "<b>Effects</b>"
msgstr "<b>ეფექტები</b>"

msgid "<b>Desktop</b>"
msgstr "<b>სამუშაო მაგიდა</b>"

msgid "<b>Accessibility</b>"
msgstr "<b>წვდომადობა</b>"

msgid "Info"
msgstr "ინფორმაცია"

msgid "Enable animations"
msgstr "ანიმაციების ჩართვა"

msgid "Enable extra animations"
msgstr "დამატებითი ანიმაციების ჩართვა"

msgid "<b>Open window</b>"
msgstr "<b>ფანჯრის გახსნა</b>"

msgid "<b>Close Window</b>"
msgstr "<b>ფანჯრის დახურვა</b>"

msgid "<b>Focus window</b>"
msgstr "<b>ფანჯრის ფოკუსი</b>"

msgid "<b>Minimize window</b>"
msgstr "<b>ფანჯრის ჩაკეცვა</b>"

msgid "Animations"
msgstr "ანიმაციები"

msgid "<b>Switcher</b>"
msgstr "<b>გადამრთველი</b>"

msgid "Deformation:"
msgstr "დეფორმაცია:"

msgid "Opacity:"
msgstr "გაუმჭვირვალობა:"

msgid "Enable Reflection"
msgstr "ანარეკლის ჩართვა"

msgid "Enable 3D Windows"
msgstr "3D ფანჯრების ჩართვა"

msgid "<b>Cube Effects</b>"
msgstr "<b>კუბის ეფექტები</b>"

msgid "Enable Scale"
msgstr "მასშტაბირების ჩართვა"

msgid "Enable Expo"
msgstr "ექსპოს ჩართვა"

msgid "Enable Blur"
msgstr "ბუნდოვნების ჩართვა"

msgid "Enable Wobbly"
msgstr "მოკანკალე ფანჯრების ჩართვა"

msgid "<b>Additions</b>"
msgstr "<b>დამატებითები</b>"

msgid "Effects"
msgstr "ეფექტები"

msgid "<b>Appearance</b>"
msgstr "გარეგნობა"

msgid "<b>Desktop columns</b>"
msgstr "<b>სამუშაო მაგიდის სვეტები</b>"

msgid "<b>Desktop rows</b>"
msgstr "<b>სამუშაო მაგიდის მწკრივები</b>"

msgid "<b>Desktop preview</b>"
msgstr "<b>სამუშაო მაგიდის მინიატურა</b>"

msgid "Desktop"
msgstr "სამაგიდო"

msgid "Screen zoom"
msgstr "ეკრანის გადიდება"

msgid "Area zoom"
msgstr "ადგილის გადიდება"

msgid "<b>Enable zoom</b>"
msgstr "<b>გადიდების ჩართვა</b>"

msgid "<b>Screen zoom</b>"
msgstr "<b>ეკრანის გადიდება</b>"

msgid "<b>Area zoom</b>"
msgstr "<b>ადგილის გადიდება</b>"

msgid "Accessibility"
msgstr "წვდომადობა"

msgid "<b>Screen Edges</b>"
msgstr "<b>ეკრანის კიდეები</b>"

msgid "Edges"
msgstr "კიდეები"

msgid "Configure Compiz with CompizConfig"
msgstr "Compiz-ის მორგება CompizConfig-ით"
